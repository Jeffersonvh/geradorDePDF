/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

/**
 *
 * @author Detroit
 */
public class certificadoBean {
    int id;
    String nome;
    String naturalidade;
    String nascimento;
    String cpf;
    String atuacao;
    int hora;

    public certificadoBean() {
    }

    public certificadoBean(int id, String nome, String naturalidade, String nascimento, String cpf, String atuacao, int hora) {
        this.id = id;
        this.nome = nome;
        this.naturalidade = naturalidade;
        this.nascimento = nascimento;
        this.cpf = cpf;
        this.atuacao = atuacao;
        this.hora = hora;
    }

    public certificadoBean(String nome, String naturalidade, String nascimento, String cpf, String atuacao, int hora) {
        this.nome = nome;
        this.naturalidade = naturalidade;
        this.nascimento = nascimento;
        this.cpf = cpf;
        this.atuacao = atuacao;
        this.hora = hora;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNaturalidade() {
        return naturalidade;
    }

    public void setNaturalidade(String naturalidade) {
        this.naturalidade = naturalidade;
    }

    public String getNascimento() {
        return nascimento;
    }

    public void setNascimento(String nascimento) {
        this.nascimento = nascimento;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getAtuacao() {
        return atuacao;
    }

    public void setAtuacao(String atuacao) {
        this.atuacao = atuacao;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }
   
}
