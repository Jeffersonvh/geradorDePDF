/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.certificadoBean;
import bean.usuarioBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Detroit
 */
public class usuarioDAO extends BaseDAO{
    
    public ArrayList<usuarioBean> select() {

        ArrayList<usuarioBean> dados = new ArrayList();

        Connection conn = novaConexao();
        String sql = "select * from usuario;";
        PreparedStatement pstmt = null;

        try {
            pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                dados.add(new usuarioBean(rs.getInt("id"), rs.getString("nome"), rs.getString("senha")));

            }
            pstmt.close();
            conn.close();

        } catch (SQLException ex) {
            Logger.getLogger(certificadoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return dados;
    }
    
    public void inserir(usuarioBean dados) {

        Connection conn = novaConexao();

        String sql = "insert into usuario(nome,senha) values (?,?);";

        PreparedStatement pstmt = null;

        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, dados.getNome());
            pstmt.setString(2, dados.getSenha());
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(usuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
