/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.certificadoBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Detroit
 */
public class certificadoDAO extends BaseDAO{
    public ArrayList<certificadoBean> select() {

        ArrayList<certificadoBean> dados = new ArrayList();

        Connection conn = novaConexao();
        String sql = "select * from certificados;";
        PreparedStatement pstmt = null;

        try {
            pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                dados.add(new certificadoBean(rs.getInt("id"), rs.getString("nom_completo"), rs.getString("naturalidade"), rs.getString("dat_Nasc"), rs.getString("cpf_Certidao"), rs.getString("atuacao"), rs.getInt("hora_Atuacao")));

            }
            pstmt.close();
            conn.close();

        } catch (SQLException ex) {
            Logger.getLogger(certificadoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return dados;
    }
    
    public ArrayList<certificadoBean> selectById(int id) {

        ArrayList<certificadoBean> dados = new ArrayList();

        Connection conn = novaConexao();
        String sql = "select * from certificados where id = "+ id +";";
        PreparedStatement pstmt = null;

        try {
            pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                dados.add(new certificadoBean(rs.getInt("id"), rs.getString("nom_completo"), rs.getString("naturalidade"), rs.getString("dat_Nasc"), rs.getString("cpf_Certidao"), rs.getString("atuacao"), rs.getInt("hora_Atuacao")));

            }
            pstmt.close();
            conn.close();

        } catch (SQLException ex) {
            Logger.getLogger(certificadoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return dados;
    }
    
    public ArrayList<certificadoBean> selectByCPF(String cpf) {

        ArrayList<certificadoBean> dados = new ArrayList();

        Connection conn = novaConexao();
        String sql = "select * from certificados where cpf_Certidao = ?;";
        PreparedStatement pstmt = null;

        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, cpf);
            
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                dados.add(new certificadoBean(rs.getInt("id"), rs.getString("nom_completo"), rs.getString("naturalidade"), rs.getString("dat_Nasc"), rs.getString("cpf_Certidao"), rs.getString("atuacao"), rs.getInt("hora_Atuacao")));

            }
            pstmt.close();
            conn.close();

        } catch (SQLException ex) {
            Logger.getLogger(certificadoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return dados;
    }
    
    public void insert(certificadoBean dados) {

       Connection conn = novaConexao();

        String sql = "INSERT INTO `tcc`.`certificados` (`nom_completo`, `naturalidade`, `dat_Nasc`, `cpf_Certidao`, `atuacao`, `hora_Atuacao`) VALUES (?,?,?,?,?,?);";

        PreparedStatement pstmt = null;

        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, dados.getNome());
            pstmt.setString(2, dados.getNaturalidade());
            pstmt.setString(3, dados.getNascimento());
            pstmt.setString(4, dados.getCpf());
            pstmt.setString(5, dados.getAtuacao());
            pstmt.setInt(6, dados.getHora());
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(usuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
