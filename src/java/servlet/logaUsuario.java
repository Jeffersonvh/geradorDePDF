/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.usuarioBean;
import dao.usuarioDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Detroit
 */
@WebServlet(name = "logaUsuario", urlPatterns = {"/logaUsuario"})
public class logaUsuario extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        usuarioBean dados = new usuarioBean(request.getParameter("login"), request.getParameter("senha"));
        
        usuarioDAO dao = new usuarioDAO();
        
        ArrayList<usuarioBean> dadosBanco = dao.select();
        
        try (PrintWriter out = response.getWriter()) {
            for (int i = 0; i < dadosBanco.size(); i++) {

                if (dadosBanco.get(i).getNome().equals(dados.getNome())) {
                    if (dadosBanco.get(i).getSenha().equals(dados.getSenha())) {

                        response.sendRedirect("./cadastraCertificado.html");
                        break;
                    }
                }
            }

            out.println("<html>");
            out.println("Usuario ou senha incorreto!");
            out.println("<META HTTP-EQUIV=\"Refresh\" content=\"0.9; URL=./logar.html\">");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
