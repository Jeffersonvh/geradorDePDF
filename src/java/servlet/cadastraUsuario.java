/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.usuarioBean;
import dao.usuarioDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Detroit
 */
@WebServlet(name = "cadastraUsuario", urlPatterns = {"/cadastraUsuario"})
public class cadastraUsuario extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        usuarioBean dados = new usuarioBean(request.getParameter("login"), request.getParameter("senha"));
        usuarioDAO dao = new usuarioDAO();

        ArrayList<usuarioBean> dadosBanco = new ArrayList<>();
        dadosBanco = dao.select();

        boolean validar = true;

        try (PrintWriter out = response.getWriter()) {

            for (int i = 0; i < dadosBanco.size(); i++) {
                if (dadosBanco.get(i).getNome().equals(dados.getNome())) {
                    out.println("<html>");
                    validar = false;
                    out.println("Usuário ja existente!");
                    out.println("<META HTTP-EQUIV=\"Refresh\" content=\"3; URL=./cadastro.html\">");
                    break;
                }
                if ("".equals(dados.getNome()) || dados.getNome() == null || "".equals(dados.getSenha()) || dados.getSenha() == null) {
                    out.println("<html>");
                    validar = false;
                    out.println("Preencha todos os campos!");
                    out.println("<META HTTP-EQUIV=\"Refresh\" content=\"3; URL=./cadastro.html\">");
                    break;
                }
                if (dados.getNome().length() < 3 || dados.getSenha().length() < 3) {
                    out.println("<html>");
                    validar = false;
                    out.println("No minimo 3 caracteres para cada campo!");
                    out.println("<META HTTP-EQUIV=\"Refresh\" content=\"3; URL=./cadastro.html\">");
                    break;
                }
            }
            if (validar == true) {
                dao.inserir(dados);
                out.println("<html>");
                out.println("Cadastro realizado com sucesso!<br>");
                out.println("Você será redirecionado, aguarde...");
                out.println("<META HTTP-EQUIV=\"Refresh\" content=\"2; URL=./listarCertificados.jsp\">");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
