/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import bean.certificadoBean;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import dao.certificadoDAO;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Detroit
 */
@WebServlet(name = "gerarCertificado", urlPatterns = {"/gerarCertificado"})
public class gerarCertificado extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String id = request.getParameter("id");

        ArrayList<certificadoBean> certificado = new certificadoDAO().selectById(Integer.parseInt(id));

        //------------começa a gerar--------------

        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream("C:\\Users\\Detroit\\Documents\\NetBeansProjects\\geradorDePDF\\web\\certificados\\"+certificado.get(0).getCpf()+".pdf"));

            document.open();
            document.add(new Paragraph("Certificamos que " + certificado.get(0).getNome() + " com o CPF de " + certificado.get(0).getCpf() + ", nascido no dia " + certificado.get(0).getNascimento() + ", natural de " + certificado.get(0).getNaturalidade() + " participou da " + certificado.get(0).getAtuacao() + ", prefazendo-se presente um total de " + certificado.get(0).getHora() + " Horas."));
        } catch (DocumentException de) {
            System.err.println(de.getMessage());
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
        document.close();

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Gerar Certificado</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("Gerando pdf, aguarde...");
            out.println("<META HTTP-EQUIV=\"Refresh\" content=\"2; URL=./certificados/"+certificado.get(0).getCpf()+".pdf\">");
            out.println("</body>");
            out.println("</html>");
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
